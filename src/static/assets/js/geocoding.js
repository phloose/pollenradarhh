
var adresse = document.getElementById("adresse_standort");
navigator.geolocation.getCurrentPosition(
    function showPosition(position){ // success cb
        console.log( position );

        var lat = position.coords.latitude;
        var lon = position.coords.longitude;
        console.log(lat, lon);

        var api_key = '58d904a497c67e00015b45fcfbcf220665694ee09fc8ec5614f7190b';

        geocode_ors = 'https://api.openrouteservice.org/geocode/reverse?api_key=' + api_key + '&point.lon=' + lon + '&point.lat=' + lat;
        console.log(geocode_ors);
        $.getJSON(geocode_ors, function(data) {
            console.log(data);
            var street_name = data.features[0].properties.name;
            var plz = data.features[0].properties.postalcode;
            var city = data.features[0].properties.locality;
			if (plz != undefined) {
				adresse.innerHTML = street_name + ', ' + plz + ' ' + city;
			} else if (plz == undefined) {
				adresse.innerHTML = street_name + ', ' + city;
			}
        });

    },
    function(){ // fail cb
        console.log("Geolocation is not suported by this browser.");
    }
);


// genutze Links
// https://shellcreeper.com/get-current-address-with-geolocation-and-google-maps-api/
// https://www.w3schools.com/html/html5_geolocation.asp
// https://stackoverflow.com/questions/10923769/simple-reverse-geocoding-using-nominatim
// http://jsfiddle.net/x5c1s6mh/2/

//bestes Ergebnis bei der geocode reverse Suche
//source_id = node/683258827
