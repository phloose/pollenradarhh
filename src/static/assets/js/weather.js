var api_key = 'f4730a50695296cd53fb908c48894ab0'; // API Key für OpenWeatherMap

// Windrichtungen kardinal und Grad
// http://snowfence.umn.edu/Components/winddirectionanddegreeswithouttable3.htm
function wind_direction(wind) {
    if (378.45 < wind && wind < 11.25 ) {
        return wind_car_dir = "N";
    } else if (11.25 < wind && wind < 33.75 ) {
        return wind_car_dir = "NEE";
    } else if (33.75 < wind && wind < 56.25 ) {
        return wind_car_dir = "NE";
    } else if (56.25 < wind && wind < 78.75 ) {
        return wind_car_dir = "ENE";
    } else if (78.75 < wind && wind < 101.25 ) {
        return wind_car_dir = "E";
    } else if (101.25 < wind && wind < 123.75 ) {
        return wind_car_dir = "ESE";
    } else if (123.75 < wind && wind < 146.25 ) {
        return wind_car_dir = "SE";
    } else if (146.25 < wind && wind < 168.75 ) {
        return wind_car_dir = "SSE";
    } else if (168.75 < wind && wind < 191.25 ) {
        return wind_car_dir = "S";
    } else if (191.25 < wind && wind < 213.75 ) {
        return wind_car_dir = "SSW";
    } else if (213.75 < wind && wind < 236.25 ) {
        return wind_car_dir = "SW";
    } else if (236.25 < wind && wind < 258.75 ) {
        return wind_car_dir = "WSW";
    } else if (258.75 < wind && wind < 281.25 ) {
        return wind_car_dir = "W";
    } else if (281.25 < wind && wind < 303.75 ) {
        return wind_car_dir = "WNW";
    } else if (303.75 < wind && wind < 326.25 ) {
        return wind_car_dir = "NW";
    } else if (326.25 < wind && wind < 348.75 ) {
        return wind_car_dir = "NNW";
    }};

// Wetterlage aus ID von openweathermap.org
function get_weather_condition(id) {
    var id2xx = /^2/;
    var id3xx = /^3/;
    var id5xx = /^5/;
    var id6xx = /^6/;
    var id7xx = /^7/;
    var id80xx = /^80/;

    if (id2xx.test(id)) {
        return weather_con = "Gewitter";
    } else if (id3xx.test(id)) {
        return weather_con = "Nieselregen";
    } else if (id5xx.test(id)) {
        return weather_con = "Regen";
    } else if (id6xx.test(id)) {
        return weather_con = "Schnee";
    } else if (id7xx.test(id)) {
        return weather_con = "Nebel";
    } else if (id == "800") {
        return weather_con = "klarer Himmel";
    } else if (id80xx.test(id)) {
        return weather_con = "Wolken"
    }
};


$.getJSON('http://api.openweathermap.org/data/2.5/weather?q=Hamburg,de&APPID=' + api_key, function(data) {
    console.log(data);
    var weather_condition_id = data.weather[0].id.toString();
    var weather_condition = get_weather_condition(weather_condition_id);
    var temp = Math.round(data.main.temp - 273.15);
    var humidity = data.main.humidity;
    var wind_speed = Math.round(data.wind.speed);
    var wind_dir = data.wind.deg;
    var wind_cardinal_direction = wind_direction(wind_dir);
    console.log(temp);
    console.log(humidity);
    console.log(wind_speed);
    console.log(wind_dir);
    document.getElementById('wetterlage_start').innerHTML = " Wetterlage: " + weather_condition;
    document.getElementById('temp_start').innerHTML = "Temperatur: " + temp + " °C";
    document.getElementById('nied_start').innerHTML = "Luftfeuchtigkeit: " + humidity + " %";
    document.getElementById('wind_start').innerHTML = "Wind: " + wind_cardinal_direction + " " + wind_speed + " km/h";
});
