
function ajax(opt, callback) {
    var url = opt.url;
    var method = opt.method || 'GET';
    var header = method === 'POST' ? 'application/x-www-form-urlencoded' : 'application/json';
    var params = opt.params || null;

    var req = new XMLHttpRequest();
    req.open(method, url, true);
    req.setRequestHeader('Content-type', header);
    if (params) {
        let param_string = '';
        for (let p in params) {
            param_string = param_string + p + '=' + params[p] + '&';
        }
        param_string = param_string.substr(0, param_string.length-1);
        req.send(param_string);
    } else {
        req.send();
    }
    req.addEventListener('load', callback);
}
