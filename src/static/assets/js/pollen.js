
var pollen_state = {};
function callDWDPollenForecast() {
    ajax({
        url: "https://cors-anywhere.herokuapp.com/https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json"
    }, function(e) {
        var json = JSON.parse(this.responseText);
        var pollen = json.content[0].Pollen;
        var of_interest = ['Birke', 'Esche', 'Erle', 'Hasel'];
        for (let polle in pollen) {
            if (of_interest.includes(polle)) {
                pollen_state[polle] = pollen[polle]['today'];
            }
        }

        for (let polle_name in pollen_state) {
            if (pollen[polle_name]['today'] == "0") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>keine";
                $('#' + polle_name + '_gefahr').css({
					//"background-color": "white",
                    //"border": "1px solid #949494",
                    //"border-radius": "10px",
                    //"color": "black"
                });
            } else if (pollen[polle_name]['today'] == "0-1") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>keine bis gering";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#fff5f0",
                });
            } else if (pollen[polle_name]['today'] == "1") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>gering";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#fee0d2",
                });
            } else if (pollen[polle_name]['today'] == "1-2") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>gering bis mittel";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#fcbba1",
                });
            } else if (pollen[polle_name]['today'] == "2") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>mittel";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#fc9272",
                });
            } else if (pollen[polle_name]['today'] == "2-3") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>mittel bis hoch";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#fb6a4a",
                });
            } else if (pollen[polle_name]['today'] == "3") {
                document.getElementById(polle_name + "_gefahr").innerHTML = polle_name + ":<br>hoch";
                $('#' + polle_name + '_gefahr').css({
                    "background-color": "#ef3b2c",
                });
            };
        }

    });
}

callDWDPollenForecast();

