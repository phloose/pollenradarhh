var raster_model;
$('#get_trees_model').click(function(e) {
    if ($('#bbx_route').is(':empty')) {
        return;
    } else {
        let bbox = $('#bbx_route').text();
        ajax({
            url: `/model?bbox=${bbox}`
        }, function(e) {
            console.log(this.responseText);
            if (raster_model) {
                map.removeLayer(raster_model);
            }
            plotty.addColorScale(
                'reddish',
                ['#fff5f0', '#fee0d2',
                 '#fcbba1', '#fc9272', '#fb6a4a', '#ef3b2c'],
                [0, 0.2, 0.4, 0.6, 0.8, 1]
            );
            let renderer = L.LeafletGeotiff.plotty({
                displayMin: 0.005,
                clampLow: false,
                colorScale: 'reddish'
            });
            raster_model = L.leafletGeotiff('static/img/model.tif', {
                name: 'Pollenflug_modell',
                renderer: renderer
            }).addTo(map);
        });
    }
});
