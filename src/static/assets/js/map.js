// ########################### Initial setup ###################################

var map = L.map('mapid', {zoomControl: false});
var positionMarker;
map.on('load', function(e) {
    navigator.geolocation.getCurrentPosition(successGeoLoc, errorGeoLoc, {
        enableHighAccuracy: true,
        maximumAge: 0
    });
});
map.setView([53.551085, 9.993682], 13);
var initial_bounds = map.getBounds().toBBoxString();
var arrow = L.icon({
    iconUrl: $ARROW,
    iconSize: [32, 32],
    iconAnchor: [16, 16]
});
var markers = [];
var trees = [];
var osm_layer = L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    maxZoom: 18,
});
osm_layer.addTo(map);

var pollen_state = {};

// Display weatherstations and call the used APIs on loading
callOpenWeatherAPI(map.getBounds().toBBoxString(), map.getZoom());
callDWDPollenForecast();

// ####################### LayerSwitcher #######################################
var baseMap = {
    "OpenStreeMap": osm_layer
};

var weatherMarkersGroup = L.layerGroup();
var treeMarkersGroup = {
    '<img src="static/img/square.png" width="18px" height="18px"/><span class="Birke">Birke</span>': L.layerGroup(),
    '<img src="static/img/circle.png" width="18px" height="18px"/><span class="Hasel">Hasel</span>': L.layerGroup(),
    '<img src="static/img/triangle.png" width="18px" height="18px"/><span class="Esche">Esche</span>': L.layerGroup(),
    '<img src="static/img/diamond.png" width="18px" height="18px"/><span class="Erle">Erle</span>': L.layerGroup()
};
var controller = L.control.layers(baseMap, {
    'Wetterstationen': weatherMarkersGroup,
    ...treeMarkersGroup
});
controller.addTo(map);

// ####################### Helper functions ####################################

// ##### Marker related
function addPositionMarker(lat, lon) {
    if (positionMarker != undefined) {
        map.removeLayer(positionMarker);
    }
    var cross = L.icon({
        iconUrl: 'static/img/position.png',
        iconSize: [32, 32],
        iconAnchor: [16, 16]
    });
    positionMarker = L.marker([lat, lon], {
        icon: cross
    });
    positionMarker.addTo(map);
}

function addStationMarker(match) {
    var station = match.name;
    var wind_dir = match.wind.deg;
    var wind_speed = match.wind.speed;

    var coords = [match.coord.Lat, match.coord.Lon];
    var marker = L.marker(coords, {
        icon: arrow,
        rotationAngle: wind_dir + 180 > 360 ? wind_dir - 180 : wind_dir + 180
    });
    markers.push(marker);
    var popupText = `<code>
                        Ort: <b>${station}</b><br>
                        Windgeschwindigkeit: <b>${wind_speed}</b><br>
                        Windrichtung: <b>${wind_dir}</b>
                    </code>`;
    marker.bindPopup(popupText);
    marker.addTo(map);
}

function removeMarkers(markers) {
    for(let i = 0; i < markers.length; i++) {
        map.removeLayer(markers[i]);
    }
}

function removeTrees() {
    map.eachLayer(function(layer) {
        if (layer.nameTag && layer.nameTag === 'TreeGeoJSON') {
            map.removeLayer(layer);
        }
    });
}

// ##### Geolocation callbacks

function successGeoLoc(position) {
    var lon = position.coords.longitude;
    var lat = position.coords.latitude;
    map.setView([lat, lon], 13);
    addPositionMarker(lat, lon);
    treesAtLoc(lat, lon);
}

function errorGeoLoc(positionError) {
    error_mapping = {
        1: 'PERMISSION_DENIED',
        2: 'POSITION_UNAVAILABLE',
        3: 'TIMEOUT'
    };
    console.error(error_mapping[positionError.code], positionError.message);
}

// ############################## External APIs ################################

function callOpenWeatherAPI(bounds, zoom) {
    ajax({
        url: `https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/box/city?bbox=${bounds},${zoom}&appid=58c63fedab06fdfea4e437f2826d7383`
    }, function(e) {
        var json = JSON.parse(this.responseText);
        matches = json.list;
        for (let i = 0; i < matches.length; i++) {
            var current = matches[i];
            addStationMarker(current);
        }
        weatherMarkersGroup.clearLayers();
        controller.removeLayer(weatherMarkersGroup);
        markers.forEach(function(marker) {
            weatherMarkersGroup.addLayer(marker);
        });
        weatherMarkersGroup.addTo(map);
        controller.addOverlay(weatherMarkersGroup, 'Wetterstationen');
    });
}


function callDWDPollenForecast() {
    ajax({
        url: "https://cors-anywhere.herokuapp.com/https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json"
    }, function(e) {
        var json = JSON.parse(this.responseText);
        var pollen = json.content[0].Pollen;
        var of_interest = ['Birke', 'Esche', 'Erle', 'Hasel'];
        for (let polle in pollen) {
            if (of_interest.includes(polle)) {
                console.log("DWD", polle, pollen[polle]['today']);
                pollen_state[polle] = pollen[polle]['today'];
            }
        }
    });
}

// ################### Map eventhandler ########################################

map.on('zoomend', function(ev) {
    removeMarkers(markers);
    var bounds = map.getBounds().toBBoxString();
    var zoom = map.getZoom();
    callOpenWeatherAPI(bounds, zoom);
});

map.on('click', function(ev) {
    removeTrees();
    if ($('#routeneingabe').css('visibility') === 'visible') {
        return;
    }
    var {lng, lat} = ev.latlng;
    addPositionMarker(lat, lng);
    treesAtLoc(lat, lng);


});

// #################### Pollenradar main functions #############################

function treesAtLoc(lat, lng) {
    ajax({
        url: `/trees_at_loc?lon=${lng}&lat=${lat}`
    }, function(e) {
        var response = this.responseText;
        var GeoJson = JSON.parse(response);
        for (const treeType in treeMarkersGroup) {
            treeMarkersGroup[treeType].clearLayers();
        }
        L.geoJSON(GeoJson, {
            pointToLayer: treePoint,
            onEachFeature: onEachFeature,
        }).addTo(map);
        for (const treeType in treeMarkersGroup) {
            treeMarkersGroup[treeType].addTo(map);
        }
    });
}

function treePoint(feature, latlng) {
    var tree = feature.properties.name;
    var pollen_severity = pollen_state[tree];
    var ps_desc = {
        '0-1':  'keine bis geringe Belastung',
        '1':    'geringe Belastung',
        '1-2':  'geringe bis mittlere Belastung',
        '2':    'mittlere Belastung',
        '2-3':  'mittlere bis hohe Belastung',
        '3':    'hohe Belastung'
    };

    var fillColor = null;
    switch(pollen_severity) {
        case '0':
            return;
        case '0-1':
            fillColor = '#fff5f0';
            break;
        case '1':
            fillColor = '#fee0d2';
            break;
        case '1-2':
            fillColor = '#fcbba1';
            break;
        case '2':
            fillColor = '#fc9272';
            break;
        case '2-3':
            fillColor = '#fb6a4a';
            break;
        case '3':
            fillColor = '#ef3b2c';
            break;
    }
    var treeMarker = {
        'Birke' : 'square',
        'Hasel' : 'circle',
        'Esche' : 'triangle',
        'Erle'  : 'diamond'
    };

    var markerOptions = {
        radius: 8,
        fillColor: fillColor,
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8,
        shape: treeMarker[tree]
    };
    var treeShape = L.shapeMarker(latlng, markerOptions);
    var popupContent = `<code>
                            ${tree} - ${ps_desc[pollen_state[tree]]}
                        </code>`;
    treeShape.bindPopup(popupContent);
    for (const treeTypeHtml in treeMarkersGroup) {
        if (treeTypeHtml.includes(tree)) {
            treeMarkersGroup[treeTypeHtml].addLayer(treeShape);
        }
    }
    return treeShape;
}

function onEachFeature(feature, layer) {
    layer.nameTag = 'TreeGeoJSON'; // tag each tree-layer
}


