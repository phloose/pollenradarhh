var startpoint_layer;
var endpoint_layer;
var way_layer;


document.getElementById('routenplanung').style.cursor = 'pointer';
document.getElementById('rt_div_close').style.cursor = 'pointer';

 // Div für Routenplanung aktiv schalten
$("#routenplanung").click(function() {
    if (legend.getPosition() == 'bottomright') {
        legend.setPosition('topleft');
    }
    $("#routeneingabe").css("visibility", "visible");
});

// Bei Klick auf die Karte Start- und Endpunkt für die Route und Route berechnen
map.on('click', function(event) {

    if ($("#routeneingabe").css("visibility") == "visible") {
        var latlng = map.mouseEventToLatLng(event.originalEvent);

        var api_key = '58d904a497c67e00015b45fcfbcf220665694ee09fc8ec5614f7190b';
        geocode_ors = 'https://api.openrouteservice.org/geocode/reverse?api_key=' + api_key + '&point.lon=' + latlng.lng + '&point.lat=' + latlng.lat;
        $.getJSON(geocode_ors, function(data) {
            var street_name = data.features[0].properties.name;
            var city = data.features[0].properties.locality;

            if ($("#start_route").html() == "Startpunkt auf der Karte auswählen!") {
                var start_marker = L.marker([latlng.lat, latlng.lng]);
				map.addLayer(start_marker);
				startpoint_layer = start_marker;
                $("#start_route").html(street_name + ', ' + city);
                $("#start_coords").html(latlng.lng + ',' + latlng.lat);
            } else if ($("#start_route").html() != "Startpunkt auf der Karte auswählen!" && $("#end_route").html() == "Endpunkt auf der Karte auswählen!") {
				var end_marker = L.marker([latlng.lat, latlng.lng]);
                map.addLayer(end_marker);
				endpoint_layer = end_marker;
                $("#end_route").html(street_name + ', ' + city);
                $("#end_coords").html(latlng.lng + ',' + latlng.lat);

                // Routenführung anzeigen lassen
                var api_key = '58d904a497c67e00015b45fcfbcf220665694ee09fc8ec5614f7190b';
                var start_coords = $("#start_coords").html();
                var end_coords = $("#end_coords").html();
                var route_ors = 'https://api.openrouteservice.org/v2/directions/foot-walking?api_key=' + api_key + '&start=' + start_coords + '&end=' + end_coords;
                $.getJSON(route_ors, function(data) {
					var route_layer = L.geoJSON(data);
					map.addLayer(route_layer);
					way_layer = route_layer;
                    console.log(data);
					$("#bbx_route").html(data.bbox[0] + "," + data.bbox[1] + "," + data.bbox[2] + "," + data.bbox[3]);
                });
            }
        });
    }

});
// onclick event coordinate
//https://gis.stackexchange.com/questions/210041/using-leaflet-js-is-it-possible-to-know-the-onclick-location-of-a-marker-ignor/210102

// Routenfenster schließen und Layer von der Karte löschen
$("#rt_div_close").click (function() {
    if (legend.getPosition() == 'topleft') {
        legend.setPosition('bottomright');
    }
    $("#routeneingabe").css("visibility", "hidden");
	$("#start_route").html("Startpunkt auf der Karte auswählen!");
	$("#end_route").html("Endpunkt auf der Karte auswählen!");
	map.removeLayer(way_layer);
	map.removeLayer(startpoint_layer);
    map.removeLayer(endpoint_layer);
    if (raster_model) {
        map.removeLayer(raster_model);
    }
	$("#bbx_route").html("");
});
