function getColor(v) {
    return v == '0-1'   ? '#fff5f0' :
           v == '1'     ? '#fee0d2' :
           v == '1-2'   ? '#fcbba1' :
           v == '2'     ? '#fc9272' :
           v == '2-3'   ? '#fb6a4a' :
           v == '3'     ? '#ef3b2c' : '';
}

var legend_desc = {
    '0-1':  'keine bis geringe Belastung',
    '1':    'geringe Belastung',
    '1-2':  'geringe bis mittlere Belastung',
    '2':    'mittlere Belastung',
    '2-3':  'mittlere bis hohe Belastung',
    '3':    'hohe Belastung'
};

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        severity = ['0-1', '1', '1-2', '2', '2-3', '3'];

    for (var i = 0; i < severity.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(severity[i]) + '"></i> ' +
            legend_desc[severity[i]] + '<br>';
    }

    return div;
};

legend.addTo(map);
