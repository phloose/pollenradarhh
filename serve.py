import sys

from flask import Flask, request, render_template, url_for, jsonify

app = Flask(
    __name__,
    static_folder='src/static',
    template_folder='src/templates',
)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/map')
def _map():
    return render_template('map.html')


@app.route('/model', methods=['GET'])
def _model():

    get_result = request.args.to_dict()
    if 'bbox' not in get_result:
        return 'No bounding box specified!'

    from utils.helpers import calc_dx_dy, calc_arr_shape

    bbox = tuple(map(float, get_result['bbox'].split(',')))
    x_shape, y_shape = calc_arr_shape(*calc_dx_dy(bbox))

    app.logger.info(f'x_shape: {x_shape}, y_shape: {y_shape}')

    if x_shape > 1200 or y_shape > 2000:
        return "Resulting raster would be too large"

    import numpy as np
    import rasterio as rio
    from utils.modelling import model_pollen

    t = np.zeros((y_shape, x_shape))
    transform = rio.transform.from_bounds(*bbox, t.shape[1], t.shape[0])

    from utils.helpers import trees_in_bbox, weather_statistics_hh
    for tree in trees_in_bbox(bbox):
        t_value, x, y = tree
        row, col = rio.transform.rowcol(transform, x, y)
        t[row, col] += t_value

    ws_low, ws_high, wd_low, wd_high = weather_statistics_hh()
    result = model_pollen(t,
                          ws_range=(ws_low, ws_high),
                          wd_range=(wd_low, wd_high))
    if t.shape != result.shape:
        return 'Input and output raster do not have the same shape!'
    result_cleaned = np.where(result < 1e-6, 0, result)  # remove tiny values

    meta = {
        'crs': {
            'init': 'epsg:4326'
        },
        'driver': 'GTiff',
        'count': 1,
        'dtype': rio.float64,
        'width': result.shape[1],
        'height': result.shape[0],
        'nodata': 0,
        'transform': transform
    }

    with rio.open('src/static/img/model.tif', 'w', **meta) as raster_output:
        raster_output.write_band(1, result_cleaned)

    return str(result_cleaned)


@app.route('/trees_at_loc', methods=['GET'])
def _trees():

    get_result = request.args.to_dict()
    try:
        lon = get_result['lon']
        lat = get_result['lat']
    except KeyError:
        return "Either 'lon' or 'lat' were not given!"

    from utils.helpers import trees_at_location
    geojson = {
        "type": "FeatureCollection",
        "features": [{
            "type": "Feature",
            "geometry": geom,
            "properties": {
                "name": name
            }
        } for name, geom in trees_at_location(lon, lat)]
    }

    return jsonify(geojson)


if __name__ == '__main__':
    app.run(port=5000, debug=True)
