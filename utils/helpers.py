"""Helpers module for coordinate stuff"""
import statistics

import numpy as np
import psycopg2
import requests

# 1 meter in degree for x-direction (M_DEG_LON) and y-direction (M_DEG_LAT).
# Needs to be multiplied by 30 to get 30 meters in degree.
M_DEG_LON = 1.4355972414711885682817422752666e-5 * 30  # pixelsize ~30m for 53°N
M_DEG_LAT = 8.997741566866716e-06 * 30

BEAUFORT_WINDSPEED = [
    (0, 0.51),
    (0.51, 2.06),
    (2.06, 3.6),
    (3.6, 5.66),
    (5.66, 8.23),
    (8.23, 11.32),
]


def calc_dx_dy(bbox):
    """Calculate the difference in degree between the extent of a given bounding
    box in x- and y-direction
    """
    if not isinstance(bbox, tuple):
        raise ValueError('Bounding Box needs to be specified as tuple')

    minx, miny, maxx, maxy = bbox

    dx = maxx - minx
    dy = maxy - miny

    return dx, dy


def calc_arr_shape(dx, dy):
    """Calculate an array shape from a given extent in degree in x- and
    y-direction
    """
    x_shape = int(dx / M_DEG_LON)
    y_shape = int(dy / M_DEG_LAT)

    return x_shape, y_shape


def dwd_json():
    """Get the actual pollen load for the day for the DWD-JSON

    This is mainly used in python modules to not rely on the equivalent
    JavaScript functions.
    """
    r = requests.get('https://opendata.dwd.'
                     'de/climate_environment/health/alerts/s31fg.json')
    dwd_json = r.json()
    sh_hh = dwd_json['content'][0]['Pollen']
    pollen_of_interest = {
        pollen: severity['today']
        for pollen, severity in sh_hh.items()
        if pollen in ['Birke', 'Esche', 'Erle', 'Hasel']
    }
    return pollen_of_interest


def __db_credentials():
    """Connection parameters for the university database

    How can this be more secure?!
    """
    return {
        'host': 'map01.local.hcu-hamburg.de',
        'port': 5432,
        'database': 'db_pollen',
        'user': 'pollen',
        'password': 'cpp#polle19'
    }


def trees_in_bbox(bbox):
    """Return a generator consiting of trees with DWD-JSON pollen load and
    coordinates that lie within a given bounding box
    """
    minx, miny, maxx, maxy = bbox
    query = f"""
            with
                trees as (
                    select name, geom from trees_view
                )
                select trees.name as name,
                    st_x(st_transform(trees.geom, 4326)) as lon, st_y(st_transform(trees.geom, 4326)) as lat from trees
                where st_within(trees.geom, st_transform(st_makeenvelope({minx}, {miny}, {maxx}, {maxy}, 4326), 25832)) = 'True'
            """
    with psycopg2.connect(**__db_credentials()) as conn:
        cur = conn.cursor()
        cur.execute(query)
        dwd_pollen = dwd_json()
        # dwd_pollen = {'Birke': 3, 'Hasel': 0, 'Esche': 2, 'Erle': 1}
        return ((float(dwd_pollen[tree]), x, y)
                for tree, x, y in cur.fetchall())


def trees_at_location(lon, lat):
    """Return a generator consisting of trees with name and GeoJSON compliant
    geometry string that lie within a radius of one kilometer around a position
    given by lon and lat
    """
    query = f"""
            with
                trees as (
                    select name, geom from trees_view
                ),
                current_location as (
                    select st_transform(st_setsrid(st_point({lon},{lat}), 4326), 25832) as coord
                )
                select trees.name as name,
                        st_asgeojson(st_transform(trees.geom, 4326))::json as geom from trees, current_location
                where st_within(trees.geom, st_buffer(current_location.coord, 1000)) = 'True'
            """
    with psycopg2.connect(**__db_credentials()) as conn:
        cur = conn.cursor()
        cur.execute(query)
        return (tree for tree in cur.fetchall())


def to_beaufort(value):
    """Convert wind speed in m/s to beaufort"""
    for i, (lower, upper) in enumerate(BEAUFORT_WINDSPEED):
        if lower <= value < upper:
            return i
    else:
        return 5


def weather_statistics_hh():
    """Call the OpenWeatherMap-API for the actual winddirection and speed

    The information is fetched from 20 weather stations around Hamburg. The mean
    and standard deviation for wind direction and speed are calculated. The
    return value is a list consisting of mean - std and mean + std for both
    wind direction and and wind speed.
    """
    zoom = 11
    bounds = "9.570465087890627,53.34317300677391," \
             "10.376586914062502,53.74261986682994"
    r = requests.get(
        f'http://api.openweathermap.org/data/2.5/box/city?bbox={bounds},{zoom}&'
        'appid=58c63fedab06fdfea4e437f2826d7383')
    weather_data = r.json()
    stations = weather_data['list']
    speed_and_dir = [
        (station['wind']['deg'], to_beaufort(station['wind']['speed']))
        for station in stations
        if station['wind'].get('speed') and station['wind'].get('deg')
    ]

    # Winddirection gives the direction from where the wind is blowing, but in
    # the model we use the direction INTO the wind is blowing, so we need to add
    # or substract 180°
    speed_and_dir = [(wind - 180 if wind + 180 > 360 else wind + 180, speed)
                     for wind, speed in speed_and_dir]

    # Use sample variance because we just use a subset of possible windvalues
    mean_deg = statistics.mean(x[0] for x in speed_and_dir)
    stddev_deg = statistics.stdev(x[0] for x in speed_and_dir)

    mean_speed = statistics.mean(x[1] for x in speed_and_dir)
    stddev_speed = statistics.stdev(x[1] for x in speed_and_dir)

    low_speed = int(mean_speed - stddev_speed)
    high_speed = int(mean_speed + stddev_speed)

    low_deg = mean_deg - stddev_deg
    high_deg = mean_deg + stddev_deg

    return [low_speed, high_speed, low_deg, high_deg]


def flip_raster(raster, direction):
    """Flip a raster according to a given main wind direction"""
    flip_mapping = {
        'NW': raster,
        'NE': np.flip(raster, 1),
        'SE': np.flip(raster, None),
        'SW': np.flip(raster, 0)
    }
    return flip_mapping[direction]


def main_winddir(raster):
    """Return a string for the main wind direction of an input raster"""
    if np.any((raster >= 90) & (raster < 180)):
        return 'NW'
    elif np.any((raster >= 180) & (raster < 270)):
        return 'NE'
    elif np.any((raster >= 270) & (raster < 360)):
        return 'SE'
    elif np.any((raster > 0) & (raster < 90)):
        return 'SW'


def main_winddir_enhanced_range(raster):
    """Return a string for the main wind direction of an input raster

    This version extends the the range by 45 degree to fetch edge cases that
    main_winddir can't handle.
    """
    if np.all((raster >= 90) & (raster <= 225)):
        return 'NW'
    elif np.all((raster >= 180) & (raster <= 315)):
        return 'NE'
    elif np.all((raster >= 270) & (raster < 405)):
        return 'SE'
    elif np.all((raster >= 0) & (raster <= 135)):
        return 'SW'
