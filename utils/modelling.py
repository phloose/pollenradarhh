import numpy as np

from utils.helpers import flip_raster, main_winddir_enhanced_range, main_winddir


def deg_norm(l, h, x):
    """Normalize a given value to a weight value between 0 and 1

    'l' and 'h' mark the low and high value between which the given degree value
    should be normalized, whereas 'x' marks the value to normalize.
    """
    # handle edge case for the southward n8 neighbor cell
    if h == 405 and x >= 0:
        x += 360
    if x < l:
        return 0
    elif x > h:
        return 0
    thrs = (l + h) / 2
    if x > thrs:
        a = h - x
        b = x - thrs
        if a > b:
            return 1 - (b / a / 2)
        else:
            return a / b / 2
    elif x < thrs:
        a = thrs - x
        b = x - l
        if a > b:
            return b / a / 2
        else:
            return 1 - (a / b / 2)
    else:
        return 1


# vectorize deg_norm to be able to use bare NumPy-Arrays as input.
vdeg_norm = np.vectorize(deg_norm, otypes=[float])


def model_pollen(trees, **kwargs):
    """Main function for modelling pollen distribution by wind

    The input raster 'trees' is a numpy array of which the cells are either
    pollen affected or not. Models the distribution of pollen from these
    position based on input parameters given by kwargs. Returns the modelled
    input raster.
    """

    kw_wd = kwargs.get('wd', 135)
    kw_ws = kwargs.get('ws', 6)

    kw_wd_range = kwargs.get('wd_range')
    kw_ws_range = kwargs.get('ws_range')

    kw_wd_random = kwargs.get('wd_random', False)
    kw_ws_random = kwargs.get('ws_random', False)

    if kw_wd_random:
        wd = np.random.randint(90, 180, trees.shape)
    elif kw_wd_range:
        if not isinstance(kw_wd_range, (list, tuple)):
            raise TypeError("Wind direction range must be a list or tuple!")
        if len(kw_wd_range) > 2:
            raise ValueError("Can't work with more than 2 values!")
        low, high = kw_wd_range
        if low > high:  # handle edge case for winddir between 315 and 45 degree
            high += 360
        wd = np.random.uniform(low, high + 1, trees.shape)
    else:
        wd = np.full(trees.shape, kw_wd)

    if kw_ws_random:
        ws = np.random.randint(0, 6, trees.shape)
    elif kw_ws_range:
        if not isinstance(kw_wd_range, (list, tuple)):
            raise TypeError("Wind speed range must be a list or tuple!")
        if len(kw_wd_range) > 2:
            raise ValueError("Can't work with more than 2 values!")
        low, high = kw_ws_range
        ws = np.random.randint(low, high + 1, trees.shape)
    else:
        ws = np.full(trees.shape, kw_ws)

    main_wd = main_winddir(wd)

    # Above we added 360 degree to be able to generate a range for 315 - 45
    # degree. This yields directions with values larger than 360 degree. Now the
    # values need to be converted back to normal degrees between 0 and 360.
    wd = np.where(wd > 360, wd - 360, wd)

    # Depending on the main wind direction the raster needs to be flipped so
    # that the main wind direction is the iteration direction from the top left.
    # This also applies for the wind direction range masks. The result raster
    # needs to be flipped back in the end.
    trees = flip_raster(trees, main_wd)
    wd = flip_raster(wd, main_wd)
    ws = flip_raster(ws, main_wd)

    # pad the initial arrays to allow 'negative' indices for N8-neighbor check
    t_p = np.pad(trees, pad_width=1, mode='constant')
    wd_p = np.pad(wd, pad_width=1, mode='constant')
    ws_p = np.pad(ws, pad_width=1, mode='constant')

    Y, X = t_p.shape

    # kernel is used to exclude the actual value from the calculation by
    # multiplying with generated N8-neighbors
    kernel = np.array([
        [1, 1, 1],
        [1, 0, 1],
        [1, 1, 1]
    ])

    # boundary values for the wind influence per neighborcell.
    # Split into two arrays to use them as seperate arguments in vdeg_norm
    wd_mask_low = flip_raster(np.array([
        [90, 135, 180],
        [45, 0, 225],
        [0, 315, 270]
    ]), main_wd)

    wd_mask_high = flip_raster(np.array([
        [180, 225, 270],
        [135, 0, 315],
        [90, 405, 360]
    ]), main_wd)

    # Iterate over every single cell from the top left to the bottom right
    for i in range(1, Y - 1):
        for j in range(1, X - 1):

            wd_norm_n8 = vdeg_norm(wd_mask_low, wd_mask_high, wd_p[i-1:i+2, j-1:j+2]) * kernel
            ws_n8 = (ws_p[i-1:i+2, j-1:j+2] / 5) * kernel
            v_n8 = t_p[i-1:i+2, j-1:j+2] * kernel
            v_ac = t_p[i][j]
            v_new = v_ac + np.sum(wd_norm_n8 * ws_n8 * v_n8)
            t_p[i][j] = v_new

    return flip_raster(t_p[1:-1, 1:-1], main_wd)  # indexing removes padding
