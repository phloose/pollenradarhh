# Pollenradar HH

Das Pollenradar für Hamburg gibt Pollenallergikern einen schnellen Überblick über die Pollenbelastung in der Nähe des eigenen Standortes und bei der Routenplanung.

Erstellt im Sommersemester 2019 von Philipp Loose, Philipp Marquis und Caroline Schuldt im Kurs *Location-based Services* des Masterstudiengangs *Geodäsie und Geoinformatik* der *HafenCity Universität Hamburg*.

## Installation
* Das PollenradarHH verwendet das Mikroframework [Flask](https://palletsprojects.com/p/flask/), welches in Python geschrieben ist.
* Als Python-Distribution [Miniconda](https://docs.conda.io/en/latest/miniconda.html) mit Python 3.7 verwenden. Dies gilt vor allem für Windows, da *conda* als Paket-Manager Abhängigkeiten, die kompiliert werden müssen (z.B. für NumPy oder GDAL) zuverlässig installiert.
* Python-Umgebung und Abhängigkeiten werden mit folgendem Befehl erstellt und installiert:
    * ``conda env create -f environment.yml``
* Die Umgebung wird mit dem Befehl `conda activate prhh` aktiviert.
* Um den Server zu starten muss `serve.py` in der vorher aktivierten `prhh`-Umgebung ausgeführt werden: `python serve.py`.
* Das Pollenradar kann nur innerhalb des HCU-Netzwerks funktionieren, da die verwendete Datenbank aus Sicherheitsgründen nicht außerhalb dieses Netzwerks erreichbar ist. Daher **MUSS** zwingend der VPN-Client von Cisco verwendet werden, um sich im HCU-Netzwerk anzumelden, bevor das Pollenradar verwendet werden kann. Alternativ kann auch das an der HCU verfügbare Netzwerk *eduroam* verwendet werden.
